package org.mowitnow.mower.service;

import org.mowitnow.mower.entity.Mower;

public interface IMowerService {

     void moveInCurrentDirection(Mower mower,int lineNumber);
     void  executeTaskCommand(Mower mower, char command,int lineNumber);

     void updatePosition(int newX, int newY, Mower mower);

}
