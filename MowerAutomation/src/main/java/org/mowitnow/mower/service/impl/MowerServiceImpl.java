package org.mowitnow.mower.service.impl;

import org.mowitnow.exceptions.InvalidMowerDirectionException;
import org.mowitnow.inputs.enums.InputCommand;
import org.mowitnow.lawn.service.ILawnService;
import org.mowitnow.mower.entity.Mower;
import org.mowitnow.exceptions.InvalidCommandException;
import org.mowitnow.exceptions.PositionOutOfAreaException;
import org.mowitnow.mower.enums.MowerDirection;
import org.mowitnow.mower.service.IMowerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;


/**
 * This class implements the logic for using the mower to mow the lawn.
 */
public class MowerServiceImpl implements IMowerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MowerServiceImpl.class);

    private final ILawnService iLawnServiceService;

    public MowerServiceImpl(ILawnService iLawnServiceService) {
        this.iLawnServiceService = iLawnServiceService;
    }

    /**
     * Updates the mower's position when it moves in a direction other than left or right.
     * @param mower Mower whose position needs to be updated.
     */
    @Override
    public void moveInCurrentDirection(Mower mower,int lineNumber) {

        LOGGER.info("--- Begin moveInCurrentDirection ---");
        int newX = mower.getX();
        int newY = mower.getY();

        switch (mower.getMowerDirection()) {
            case N -> newY += 1;
            case E -> newX += 1;
            case S -> newY -= 1;
            case W -> newX -= 1;
            default -> {
                LOGGER.warn("Invalide mower direction : " + mower.getMowerDirection() + " at line  " + lineNumber + " The valid directions are " + Arrays.toString(MowerDirection.values()));
                System.out.println ("Direction invalide : " + mower.getMowerDirection() + " à la ligne " + lineNumber + " Les directions valides sont " + Arrays.toString(MowerDirection.values()) + "nous allons passer aux instructions suivantes");
                return;
            }
        }

        updatePosition(newX, newY, mower);

        LOGGER.info("--- End moveInCurrentDirection ---");
    }

    /**
     * Executes the given command to steer the mower.
     * @param mower Mower to be steered.
     * @param command Command to be executed.
     * @throws IllegalArgumentException if the provided command is invalid.
     */

    @Override
    public void executeTaskCommand(Mower mower, char command,int lineNumber) {
        LOGGER.info("----- Begin executeTaskCommand with commande " + command + " ------");
        switch (command) {
            case 'A' -> moveInCurrentDirection(mower,lineNumber);
            case 'D' -> mower.setMowerDirection(mower.getMowerDirection().rotateRight());
            case 'G' -> mower.setMowerDirection(mower.getMowerDirection().rotateLeft());
            default -> throw new InvalidCommandException("Commande invalide : " + command + " à la ligne " + lineNumber + " Les commandes valides sont " + Arrays.toString(InputCommand.values()));

        }
        LOGGER.info("----- End executeTaskCommand with commande " + command + " ------");
    }


    /**
     * Updates the mower's position based on the provided new coordinates.
     *
     * @param newX The new X coordinate where the mower should be positioned.
     * @param newY The new Y coordinate where the mower should be positioned.
     * @param mower The mower object whose position needs to be updated.
     *
     * @throws PositionOutOfAreaException if the new position is outside the lawn's boundaries.
     **/
    @Override
    public void updatePosition(int newX, int newY, Mower mower) {
        if (!iLawnServiceService.isValidPosition(newX, newY,mower.getLawn() )) {
            LOGGER.warn(String.format("--- Error during mower's position update in updatePosition function with param %s %s ---", newX, newY));
            System.out.println("La tondeuse précédente a reçu de nouvelles commandes qui la sortiraient de la pelouse. Ces commandes seront ignorées.");
            return;
          }
            mower.setX(newX);
            mower.setY(newY);
        }


}