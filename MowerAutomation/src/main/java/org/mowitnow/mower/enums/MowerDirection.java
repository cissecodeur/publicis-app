package org.mowitnow.mower.enums;

/**
 * Enum representing the cardinal directions in which a mower can face.
 *
 * N - North
 * E - East
 * S - South
 * W - West
 */
public enum MowerDirection {

    N,
    E,
    S,
    W;

    /**
     * Rotates the mower to the left and returns the new direction.
     *
     * @return MowerDirection - The new direction after rotating left.
     */
    public MowerDirection rotateLeft() {
        return values()[(ordinal() - 1 + values().length) % values().length];
    }

    /**
     * Rotates the mower to the right and returns the new direction.
     *
     * @return MowerDirection - The new direction after rotating right.
     */
    public MowerDirection rotateRight() {
        return values()[(ordinal() + 1) % values().length];
    }
}
