package org.mowitnow.mower.entity;

import org.mowitnow.exceptions.InvalidMowerDirectionException;
import org.mowitnow.exceptions.PositionOutOfAreaException;
import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.mower.enums.MowerDirection;

public class Mower {
    private int x, y;
    private MowerDirection mowerDirection;
    private final Lawn lawn;

    /**
     * Constructs a Mower with the given initial position, direction and lawn.
     * Validates the position and direction before assigning them.
     */
    public Mower(int x, int y, MowerDirection mowerDirection, Lawn lawn) {
        validatePosition(x, y, lawn);
        validateDirection(mowerDirection.name());
        this.x = x;
        this.y = y;
        this.mowerDirection = mowerDirection;
        this.lawn = lawn;
    }

    // Getter for the x-coordinate.
    public int getX() {
        return x;
    }

    // Setter for the x-coordinate.
    public void setX(int x) {
        this.x = x;
    }

    // Getter for the y-coordinate.
    public int getY() {
        return y;
    }

    // Setter for the y-coordinate.
    public void setY(int y) {
        this.y = y;
    }

    // Getter for the mower's direction.
    public MowerDirection getMowerDirection() {
        return mowerDirection;
    }

    // Setter for the mower's direction.
    public void setMowerDirection(MowerDirection mowerDirection) {
        this.mowerDirection = mowerDirection;
    }

    // Getter for the lawn on which the mower operates.
    public Lawn getLawn() {
        return lawn;
    }

    /**
     * Validates if the given x and y coordinates are within the valid boundaries of the lawn.
     * Throws a PositionOutOfAreaException if not valid.
     */
    private void validatePosition(int x, int y, Lawn lawn) {
        if(!areCoordinatesValid(x, y, lawn.width(), lawn.height()) || x > lawn.width() || y > lawn.height()) {
            throw new PositionOutOfAreaException();
        }
    }

    /**
     * Checks if the given x and y coordinates, along with the width and height, are valid (non-negative).
     * Returns true if all coordinates are valid, false otherwise.
     */
    private boolean areCoordinatesValid(int x, int y, int width, int height) {
        return x >= 0 && y >= 0 && width >= 0 && height >= 0;
    }

    /**
     * Validates if the provided direction is one of the valid mower directions.
     * Throws an InvalidMowerDirectionException if the direction is invalid.
     */
    private void validateDirection(String direction) {
        for (MowerDirection mowerDirection : MowerDirection.values()) {
            if (direction.equals(mowerDirection.name())) {
                return;
            }
        }
        throw new InvalidMowerDirectionException("Invalid mower direction: " + direction);
    }
}
