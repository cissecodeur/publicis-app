package org.mowitnow.inputs.impl;

import org.mowitnow.factory.MowingEquipmentFactory;
import org.mowitnow.inputs.enums.InputCommand;
import org.mowitnow.inputs.service.IFileService;
import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.mower.entity.Mower;
import org.mowitnow.mower.service.IMowerService;
import org.mowitnow.utils.FileValidator;
import org.mowitnow.utils.InputFileReader;
import org.mowitnow.utils.MowerParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileServiceImpl implements IFileService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);


    /**
     * Reads and processes a given input file and returns the final positions of the mowers.
     *
     * @param filePath Path of the input file to process.
     * @return List of final positions of the mowers.
     * @throws IOException If an error occurs during file reading.
     */
    public List<String> processFile(String filePath) throws IOException {
        LOGGER.info("--- Begin input file processing --- ");

        MowingEquipmentFactory mowingEquipmentFactory = new MowingEquipmentFactory();
        List<String> mowersPositions = new ArrayList<>();
        InputFileReader inputFileReader = new InputFileReader();

        int mowerNumber = 1; // mower number
        int lineNumber = 1; // number of line in file to process
        FileValidator fileValidator = new FileValidator();
        try (BufferedReader in = inputFileReader.readMyInputFile(filePath)){
            String lineToVeriFy = in.readLine();
            fileValidator.isFileNotEmpty(lineToVeriFy);
            String[] lawnDimensions = lineToVeriFy.split(" ");

            // Create lawn and mowerService using MowingEquipmentFactory
            Lawn lawn =  initializeLawn(mowingEquipmentFactory,lawnDimensions);
            IMowerService mowerService = initializeMowerService(mowingEquipmentFactory);

            String line;
            Mower mower = null;
            while ((line = in.readLine()) != null) {
                lineNumber ++ ;
                CommandProcessorServiceImpl commandProcessorServiceImpl = createCommandProcessorService();
                if (!line.isBlank()) {
                    // Process Mower with more than One sequences
                    if(isCommandSequence(line)  && mower != null ){
                        int mowerNumberNew = mowerNumber -1;
                        processMultipleCommandSequences(mowersPositions,  commandProcessorServiceImpl,  mower,  line,  mowerService,  mowerNumberNew,  lineNumber,lawn);

                    }
                    else{
                        // Process new mower sequence
                      mower =  processSingleCommandSequence( mowingEquipmentFactory,mowersPositions,  lawn,  line,  in, mowerService,  mowerNumber,  lineNumber) ;
                            mowerNumber++;

                    }

                }

            }
        }
        LOGGER.info("--- End input file processing --- ");
        return mowersPositions;
    }

    /**
     * Validates that the provided file has content.
     */
    private void validateFile(FileValidator fileValidator, BufferedReader in) throws IOException {
        String lineToVerify = in.readLine();
        fileValidator.isFileNotEmpty(lineToVerify);
    }

    /**
     * Processes a single command sequence for a mower.
     */
    private Mower processSingleCommandSequence(MowingEquipmentFactory mowingEquipmentFactory, List<String> mowersPositions, Lawn lawn, String line, BufferedReader in, IMowerService mowerService, int mowerNumber, int lineNumber) throws IOException {
        String[] mowerInit = line.split(" ");
        Mower mower = mowingEquipmentFactory.createMower(mowerInit, lawn, lineNumber+1);
        String commands = in.readLine();
        if (commands != null) {
            CommandProcessorServiceImpl commandProcessorServiceImpl = createCommandProcessorService();
            commandProcessorServiceImpl.process(mower, commands, mowerService, lineNumber+1);
            displayFinalPosition(mower, mowerNumber);
            mowersPositions.add(formatMowerFinalPosition(mower));
        }
        return mower;
    }

    /**
     * Processes multiple command sequences for a mower.
     */
    private void processMultipleCommandSequences(List<String> mowersPositions, CommandProcessorServiceImpl commandProcessorServiceImpl, Mower mower, String line, IMowerService mowerService, int mowerNumber, int lineNumber,Lawn lawn) {
        commandProcessorServiceImpl.process(mower, line, mowerService,lineNumber+1);
        mowersPositions.add(formatMowerFinalPosition(mower));
       if(MowerParamsValidator.isValidPosition(mower,lawn)){
            displayFinalPositionMultiProcess(mower,mowerNumber);
        }

    }

    /**
     * Checks if the new given line is a command sequence for the old mower.
     *
     * @param line The line to check.
     * @return true if the line is a command sequence, false otherwise.
     */
    private boolean isCommandSequence(String line) {
        for (InputCommand command : InputCommand.values()) {
            if (line.startsWith(command.name())) {
                return true;
            }
        }
        return false;
    }

    private String formatMowerFinalPosition(Mower mower) {
        return mower.getX() + " " + mower.getY() + " " + mower.getMowerDirection();
    }

    // function to create new Lawn with from mowingEquipmentFactory
    private Lawn initializeLawn (MowingEquipmentFactory mowingEquipmentFactory,String[] lawnDimensions){
        return mowingEquipmentFactory.createLawn(lawnDimensions);
    }

    // function to create new Lawn with from mowingEquipmentFactory
    private IMowerService initializeMowerService (MowingEquipmentFactory mowingEquipmentFactory){
        return  mowingEquipmentFactory.createMowerService();
    }


    private void displayFinalPosition(Mower mower, int mowerNumber) {
        System.out.println("La position finale de la tondeuse(" + mowerNumber + ") est : " + formatMowerFinalPosition(mower));
    }
    private void displayFinalPositionMultiProcess(Mower mower, int mowerNumber) {
        System.out.println("La position finale de la tondeuse(" + mowerNumber + ") à été modifiée par de nouvelle(s) commande(s), sa position definitive est donc passée à : " + formatMowerFinalPosition(mower));
    }


    /**
     * Factory method to create a new instance of CommandProcessorServiceImpl.
     * Helps in mocking during testing.
     *
     * @return A new instance of CommandProcessorServiceImpl.
     */
    public CommandProcessorServiceImpl createCommandProcessorService() {
        return new CommandProcessorServiceImpl();
    }


}
