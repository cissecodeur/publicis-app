package org.mowitnow.inputs.impl;

import org.mowitnow.mower.entity.Mower;
import org.mowitnow.mower.service.IMowerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CommandProcessorServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandProcessorServiceImpl.class);

    /*
     * I use this function to process the commands launch in the main file
     */
    public void process(Mower mower, String commands , IMowerService mowerService , int lineNumber) {
        for (char command : commands.toCharArray()) {

            mowerService.executeTaskCommand(mower, command,lineNumber);
            LOGGER.info("-- current position -- : " + mower.getX() + " " + mower.getY() + " " + mower.getMowerDirection());
        }
    }
}
