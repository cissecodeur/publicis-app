package org.mowitnow.inputs.service;

import java.io.IOException;
import java.util.List;

public interface IFileService {

       List<String> processFile(String filePath) throws IOException;
}
