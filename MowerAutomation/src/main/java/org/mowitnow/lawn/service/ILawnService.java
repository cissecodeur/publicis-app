package org.mowitnow.lawn.service;

import org.mowitnow.lawn.entity.Lawn;

public interface ILawnService {

     boolean isValidPosition(int x , int y, Lawn lawn);
}
