package org.mowitnow.lawn.service.impl;

import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.lawn.service.ILawnService;

/**
 * This class implements the logic for the lawn.
 */
public class LawnServiceImpl implements ILawnService {

    @Override
    public boolean isValidPosition(int x, int y, Lawn lawn) {
        return areCoordinatesValid(x, y, lawn.width(), lawn.height())
                && x <= lawn.width() && y <= lawn.height();
    }


    private boolean areCoordinatesValid(int x, int y, int width, int height) {
        return x >= 0 && y >= 0 && width >= 0 && height >= 0;
    }


}
