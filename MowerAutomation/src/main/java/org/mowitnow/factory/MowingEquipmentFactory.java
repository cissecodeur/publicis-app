package org.mowitnow.factory;

import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.lawn.service.ILawnService;
import org.mowitnow.lawn.service.impl.LawnServiceImpl;
import org.mowitnow.mower.entity.Mower;
import org.mowitnow.mower.enums.MowerDirection;
import org.mowitnow.mower.service.IMowerService;
import org.mowitnow.mower.service.impl.MowerServiceImpl;
import org.mowitnow.utils.MowerParamsValidator;

public class MowingEquipmentFactory {

    public  Lawn createLawn(String[] lawnDimensions) {
        int lawnWidth = Integer.parseInt(lawnDimensions[0]);
        int lawnHeight = Integer.parseInt(lawnDimensions[1]);
        return new Lawn(lawnWidth, lawnHeight);
    }

    public  IMowerService createMowerService() {
        ILawnService lawnService = new LawnServiceImpl();
        return new MowerServiceImpl(lawnService);
    }

    public  Mower createMower(String[] mowerInit, Lawn lawn, int lineNumber) {
        MowerParamsValidator.validateParamLength(mowerInit,lineNumber);
        int mowerX = MowerParamsValidator.getParamX(mowerInit);
        int mowerY = MowerParamsValidator.getParamY(mowerInit);
        MowerParamsValidator.validateDirection(mowerInit,lineNumber);

        MowerDirection direction = MowerDirection.valueOf(mowerInit[2]);
        return new Mower(mowerX, mowerY, direction, lawn);
    }
}
