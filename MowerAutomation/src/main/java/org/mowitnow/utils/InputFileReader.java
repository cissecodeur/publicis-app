package org.mowitnow.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class InputFileReader {

    // File function Validate and return the fileContent
    public  BufferedReader readMyInputFile(String filePath) throws FileNotFoundException {
        FileValidator fileValidator = new FileValidator();
        fileValidator.doesLinkPointToTxtFile(filePath);
        FileReader fileReader = new FileReader(filePath);
        return new BufferedReader(fileReader);
    }


}
