package org.mowitnow.utils;

import org.mowitnow.exceptions.InputFileEmptyException;
import org.mowitnow.exceptions.InputFileFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public  class FileValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileValidator.class);

    public boolean doesLinkPointToTxtFile(String link) {
        if (!link.endsWith(".txt")) {
            LOGGER.error("--- Error,invalid file type. You must provide a .txt --- ");
            throw new InputFileFormatException();
        }
        return true;
    }

   public void isFileNotEmpty(String lineToVeriFy){

       if(lineToVeriFy == null || lineToVeriFy.isBlank()){
           LOGGER.error("--- Error,file provided is empty --- ");
           throw new InputFileEmptyException();
       }
   }
}
