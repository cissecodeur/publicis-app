package org.mowitnow.utils;

import org.mowitnow.exceptions.InvalidMowerCoordinateException;
import org.mowitnow.exceptions.InvalidMowerDirectionException;
import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.mower.entity.Mower;
import org.mowitnow.mower.enums.MowerDirection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class MowerParamsValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(MowerParamsValidator.class);

    public static int getParamX(String[] mowerInit){
        if (mowerInit[0] == null || mowerInit[0].isEmpty()) {
            LOGGER.error("--- Error,argument X not found in this file --- ");
            throw new InvalidMowerCoordinateException("Coordonnée X de la tondeuse manquante ou vide.");
        }
        return Integer.parseInt(mowerInit[0]);
    }

    public static  int getParamY(String[] mowerInit){
        if (mowerInit[1] == null || mowerInit[1].isEmpty()) {
            LOGGER.error("--- Error,argument Y not found in this file --- ");
            throw new InvalidMowerCoordinateException("Coordonnée Y de la tondeuse manquante ou vide.");
        }
       return  Integer.parseInt(mowerInit[1]);
    }

    public static void validateParamLength(String[] mowerInit , int lineNumber){
        if (mowerInit.length < 3) {
            LOGGER.error("--- Error, this mower can't be initialize,missing argument(s) --- ");
            throw new InvalidMowerCoordinateException("Informations insuffisantes pour initialiser la tondeuse, l'erreur est survenu à la ligne " + lineNumber);
        }
    }

    public static void validateDirection(String[] mowerInit, int lineNumber){
        if (mowerInit[2] == null || mowerInit[2].isEmpty()) {
            LOGGER.error("--- Error, mower direction not found  --- ");
            throw new InvalidMowerDirectionException("Direction de la tondeuse manquante ou vide.");
        }
        validateDirection(mowerInit[2],lineNumber);
    }

    /**
     * Validates if the provided direction is one of the valid mower directions.
     * Throws an InvalidMowerDirectionException if the direction is invalid.
     */
    private static void validateDirection(String direction , int lineNumber ) {
        for (MowerDirection mowerDirection : MowerDirection.values()) {
            if (direction.equals(mowerDirection.name())) {
                return;
            }
        }
        throw new InvalidMowerDirectionException("direction invalide : " + direction + " à la ligne " + (lineNumber-1) + " Les directions valides sont " + Arrays.toString(MowerDirection.values()));
    }

    public static boolean isValidPosition(Mower mower, Lawn lawn) {
        return areCoordinatesValid(mower.getX(), mower.getY(), lawn.width(), lawn.height())
                && mower.getX() <= lawn.width() &&  mower.getY() <= lawn.height();
    }


    private static boolean areCoordinatesValid(int x, int y, int width, int height) {
        return x >= 0 && y >= 0 && width >= 0 && height >= 0;
    }


}
