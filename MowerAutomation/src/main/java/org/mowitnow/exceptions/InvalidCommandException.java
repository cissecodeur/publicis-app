package org.mowitnow.exceptions;

public class InvalidCommandException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception indicating an issue with invalid command in the input file.
     */
    public InvalidCommandException() {
        super("La commande fournie est invalide.");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public InvalidCommandException(String message){
        super(message);
    }

}
