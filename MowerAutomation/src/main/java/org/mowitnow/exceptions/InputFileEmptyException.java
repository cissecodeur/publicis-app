package org.mowitnow.exceptions;

public class InputFileEmptyException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception indicating an issue if the input file is empty.
     */
    public InputFileEmptyException() {
        super("Le fichier fourni est vide.");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public InputFileEmptyException(String message){
        super(message);
    }

}
