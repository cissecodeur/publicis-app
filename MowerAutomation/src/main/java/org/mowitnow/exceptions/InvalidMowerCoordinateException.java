package org.mowitnow.exceptions;

public class InvalidMowerCoordinateException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception indicating an issue if invalid coordinates is given with mower infos in the file.
     */
    public InvalidMowerCoordinateException() {
        super("Une des coordonnées X et/ou Y est invalide");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public InvalidMowerCoordinateException(String message){
        super(message);
    }

}
