package org.mowitnow.exceptions;

public class PositionOutOfAreaException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception when a mower is out of lawn.
     */
    public PositionOutOfAreaException() {
        super("La tondeuse est positionnée en dehors des limites de la pelouse.");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public PositionOutOfAreaException(String message){
        super(message);
    }



}