package org.mowitnow.exceptions;

public class InputFileFormatException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception indicating an issue with the input file format.
     */
    public InputFileFormatException() {
        super("Veuillez fournir un fichier texte.");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public InputFileFormatException(String message){
        super(message);
    }


}
