package org.mowitnow.exceptions;

public class InvalidMowerDirectionException extends RuntimeException{

    /**
     * Default constructor.
     * Throws an exception indicating an issue if invalid direction is given with mower infos in the file.
     */
    public InvalidMowerDirectionException() {
        super("La direction fournie pour la tondeuse est invalide");
    }

    /**
     * Constructor that accepts a custom error message.
     *
     * @param message Custom error message to be displayed when the exception is thrown.
     */
    public InvalidMowerDirectionException(String message){
        super(message);
    }

}
