package org.mowitnow;

import org.mowitnow.inputs.impl.FileServiceImpl;
import org.mowitnow.inputs.service.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Scanner;

public class MowItNow {

    private static final Logger LOGGER = LoggerFactory.getLogger(MowItNow.class);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean continueProcessing;

        do {
            System.out.println("Veuillez entrer le chemin complet du fichier à injecter (fichier.txt):");
            LOGGER.info("--- Begin main program execution. ---");

            String filePath = scanner.nextLine();

            try {
                IFileService fileService = new FileServiceImpl();
                fileService.processFile(filePath); // movement processing

                } catch (Exception e) {
                LOGGER.error("--- Error during file downloading or processing --- : " + e.getMessage());
                System.out.println("Une erreur est survenue lors du traitement de votre fichier: " + e.getMessage());
              }

            System.out.println("Voulez-vous continuer et traiter un autre fichier ? (O/N)");
            continueProcessing = scanner.nextLine().trim().equalsIgnoreCase("O");

        } while (continueProcessing);

        LOGGER.info("--- End main program execution. ---");
        System.out.println("Merci d'avoir utilisé le programme et à bientôt!");
        scanner.close();
    }
}