package org.mowitnow.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mowitnow.exceptions.InvalidMowerCoordinateException;
import org.mowitnow.exceptions.InvalidMowerDirectionException;

import static org.junit.jupiter.api.Assertions.*;

class MowerParamsValidatorTest {

    private String[] validMowerInit;
    private String[] invalidMowerInitLength;
    private String[] invalidMowerInitX;
    private String[] invalidMowerInitY;
    private String[] invalidMowerInitDirection;

    @BeforeEach
    void setUp() {
        validMowerInit = new String[]{"1", "2", "N"};
        invalidMowerInitLength = new String[]{"1", "2"};
        invalidMowerInitX = new String[]{"", "2", "N"};
        invalidMowerInitY = new String[]{"1", "", "N"};
        invalidMowerInitDirection = new String[]{"1", "2", "Z"};
    }

    @Test
    @DisplayName("Get valid X coordinate from mower parameters")
    void getParamX_valid() {
        assertEquals(1, MowerParamsValidator.getParamX(validMowerInit));
    }

    @Test
    @DisplayName("Get invalid X coordinate from mower parameters")
    void getParamX_invalid() {
        assertThrows(InvalidMowerCoordinateException.class, () -> MowerParamsValidator.getParamX(invalidMowerInitX));
    }

    @Test
    @DisplayName("Get valid Y coordinate from mower parameters")
    void getParamY_valid() {
        assertEquals(2, MowerParamsValidator.getParamY(validMowerInit));
    }

    @Test
    @DisplayName("Get invalid Y coordinate from mower parameters")
    void getParamY_invalid() {
        assertThrows(InvalidMowerCoordinateException.class, () -> MowerParamsValidator.getParamY(invalidMowerInitY));
    }

    @Test
    @DisplayName("Validate correct parameter length for mower initialization")
    void validateParamLength_valid() {
        assertDoesNotThrow(() -> MowerParamsValidator.validateParamLength(validMowerInit, 1));
    }

    @Test
    @DisplayName("Validate incorrect parameter length for mower initialization")
    void validateParamLength_invalid() {
        assertThrows(InvalidMowerCoordinateException.class, () -> MowerParamsValidator.validateParamLength(invalidMowerInitLength, 1));
    }

    @Test
    @DisplayName("Validate correct direction from mower parameters")
    void validateDirection_valid() {
        assertDoesNotThrow(() -> MowerParamsValidator.validateDirection(validMowerInit, 1));
    }

    @Test
    @DisplayName("Validate incorrect direction from mower parameters")
    void validateDirection_invalid() {
        assertThrows(InvalidMowerDirectionException.class, () -> MowerParamsValidator.validateDirection(invalidMowerInitDirection, 1));
    }
}
