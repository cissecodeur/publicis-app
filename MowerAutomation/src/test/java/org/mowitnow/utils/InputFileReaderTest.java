package org.mowitnow.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mowitnow.exceptions.InputFileFormatException;

import static org.junit.jupiter.api.Assertions.*;

class InputFileReaderTest {

    private InputFileReader inputFileReader;

    @BeforeEach
    void setUp() {
        inputFileReader = new InputFileReader();
    }

    String testFilePath = "/Users/ray/Desktop/Publicis/files/testFile.txt";
    String invalidTestFilePath = "/Users/ray/Desktop/Publicis/files";
    String invalidTestFileType = "/Users/ray/Desktop/Publicis/files/emptyTestFile.pdf";

    @Test
    @DisplayName("Read input file with valid path")
    void readMyInputFile_validPath() {
        assertDoesNotThrow(() -> inputFileReader.readMyInputFile(testFilePath));
    }

    @Test
    @DisplayName("Attempt to read input file with non-existing path")
    void readMyInputFile_invalidPath() {
        assertThrows(InputFileFormatException.class, () -> inputFileReader.readMyInputFile(invalidTestFilePath));
    }

    @Test
    @DisplayName("Attempt to read input file with invalid file type")
    void readMyInputFile_invalidFileType() {
        assertThrows(InputFileFormatException.class, () -> inputFileReader.readMyInputFile(invalidTestFileType));
    }
}
