package org.mowitnow.utils;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mowitnow.exceptions.InputFileFormatException;

class FileValidatorTest {

    private FileValidator fileValidator;

    @BeforeEach
    void setUp() {
        fileValidator = new FileValidator();
    }

    String validTxtFilePath = "/Users/ray/Desktop/Publicis/files/testFile.txt";
    String invalidTxtFilePath = "/Users/ray/Desktop/Publicis";

    @Test
    @DisplayName("Validate file with .txt extension without exceptions")
    void whenFileIsTxt_NoExceptionIsThrown() {
        assertDoesNotThrow(() -> fileValidator.doesLinkPointToTxtFile(validTxtFilePath));
    }

    @Test
    @DisplayName("Throw exception when file doesn't have a .txt extension")
    void whenFileIsNotTxt_ExceptionIsThrown() {
        assertThrows(InputFileFormatException.class, () -> fileValidator.doesLinkPointToTxtFile(invalidTxtFilePath));
    }
}
