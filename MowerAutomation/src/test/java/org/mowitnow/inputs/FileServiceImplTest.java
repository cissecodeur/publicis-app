package org.mowitnow.inputs;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mowitnow.exceptions.InputFileEmptyException;
import org.mowitnow.exceptions.InvalidCommandException;
import org.mowitnow.inputs.impl.FileServiceImpl;
import org.mowitnow.utils.InputFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class FileServiceImplTest {

    @InjectMocks
    private FileServiceImpl fileService;

    @Mock
    private BufferedReader bufferedReaderMock;

    @Mock
    private InputFileReader inputFileReaderMock;

    String testFilePath = "/Users/ray/Desktop/Publicis-App/files/testFile.txt";
    String invalidTestFilePath = "/Users/ray/Desktop/Publicis-App/files/invalidCommandTestFile.txt";
    String emptyTestFilePath = "/Users/ray/Desktop/Publicis-App/files/emptyTestFile.txt";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach // close bufferedReaderMock after each test
    void tearDown() {
        try {
            bufferedReaderMock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Process file should return correct mowers' final positions")
    void processFile_Should_ReturnCorrectFinalPositions() throws IOException {
        when(inputFileReaderMock.readMyInputFile(testFilePath)).thenReturn(bufferedReaderMock);
        List<String> finalPositions;
        finalPositions = fileService.processFile(testFilePath);

        assertEquals(2, finalPositions.size(), "The number of mowers' positions is incorrect");
        assertEquals("1 3 N", finalPositions.get(0), "First mower's final position is incorrect");
        assertEquals("5 1 E", finalPositions.get(1), "Second mower's final position is incorrect");
    }

    @Test
    @DisplayName("Process file with invalid commands should throw InvalidCommandException")
    void processFile_Should_ReturnInvalidCommands() throws IOException {
        when(inputFileReaderMock.readMyInputFile(invalidTestFilePath)).thenReturn(bufferedReaderMock);
        assertThrows(InvalidCommandException.class, () -> fileService.processFile(invalidTestFilePath));
    }

    @Test
    @DisplayName("Process an empty file should throw InputFileEmptyException")
    void processFile_Should_HandleEmptyFile() throws IOException {
        when(inputFileReaderMock.readMyInputFile(emptyTestFilePath)).thenReturn(bufferedReaderMock);
        assertThrows(InputFileEmptyException.class, () -> fileService.processFile(emptyTestFilePath));
    }
}
