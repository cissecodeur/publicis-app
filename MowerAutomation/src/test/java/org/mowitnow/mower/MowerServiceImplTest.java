package org.mowitnow.mower;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.lawn.service.ILawnService;
import org.mowitnow.mower.entity.Mower;
import org.mowitnow.mower.enums.MowerDirection;
import org.mowitnow.exceptions.InvalidCommandException;
import org.mowitnow.mower.service.impl.MowerServiceImpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

public class MowerServiceImplTest {

    @Mock
    private ILawnService lawnService;

    @InjectMocks
    private MowerServiceImpl mowerService;

    @BeforeEach
    public void setup() {
        lawnService = Mockito.mock(ILawnService.class);
        mowerService = new MowerServiceImpl(lawnService);
        when(lawnService.isValidPosition(Mockito.anyInt(), Mockito.anyInt(), Mockito.any())).thenReturn(true); // mock to always return true
    }

    @Test
    @DisplayName("Mower moves north when facing north")
    public void moveInCurrentDirection_Should_MoveNorth() {
        Mower mower = new Mower(2, 2, MowerDirection.N, new Lawn(5, 5));
        mowerService.moveInCurrentDirection(mower,2);
        assertEquals(2, mower.getX());
        assertEquals(3, mower.getY());
    }

    @Test
    @DisplayName("Mower rotates to the right when given a 'D' command")
    public void executeTaskCommand_Should_RotateMowerToRight() {
        Mower mower = new Mower(2, 2, MowerDirection.N, new Lawn(2, 5));
        mowerService.executeTaskCommand(mower, 'D',3);
        assertEquals(MowerDirection.E, mower.getMowerDirection());
    }

    @Test
    @DisplayName("Mower rotates to the left when given a 'G' command")
    public void executeTaskCommand_ShouldRotateMowerToLeft() {
        Mower mower = new Mower(2, 2, MowerDirection.N, new Lawn(5, 5));
        mowerService.executeTaskCommand(mower, 'G',3);
        assertEquals(MowerDirection.W, mower.getMowerDirection());
    }

    @Test
    @DisplayName("Invalid command ('X') should throw an exception")
    public void executeTaskCommand_Should_ThrowInvalidCommandException() {
        Mower mower = new Mower(2, 2, MowerDirection.N, new Lawn(5, 5));
        assertThrows(InvalidCommandException.class, () -> mowerService.executeTaskCommand(mower, 'X',3));
    }

    @Test
    @DisplayName("Updating to an invalid position should continue the process")
    public void updatePosition_Should_NotThrowPositionOutOfAreaException() {
        Mower mower = new Mower(2, 2, MowerDirection.N, new Lawn(5, 5));
        when(lawnService.isValidPosition(2, 6, mower.getLawn())).thenReturn(false);
        assertDoesNotThrow(() -> mowerService.updatePosition(2, 6, mower));
    }
}
