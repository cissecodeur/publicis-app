package org.mowitnow.lawn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mowitnow.lawn.entity.Lawn;
import org.mowitnow.lawn.service.impl.LawnServiceImpl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LawnServiceImplTest {

    private Lawn lawn;
    private LawnServiceImpl lawnService;

    @BeforeEach
    void setUp() {
        lawn = new Lawn(5, 5);
        lawnService = new LawnServiceImpl();
    }

    @Test
    @DisplayName("Position inside the lawn should be valid")
    void isValidPosition_InsideLawn_ReturnsTrue() {
        assertTrue(lawnService.isValidPosition(3, 3, lawn));
    }

    @Test
    @DisplayName("Position at the lawn's boundary should be valid")
    void isValidPosition_AtLawnBoundary_ReturnsTrue() {
        assertTrue(lawnService.isValidPosition(5, 5, lawn));
    }

    @Test
    @DisplayName("Position outside the lawn should be invalid")
    void isValidPosition_OutsideLawn_ReturnsFalse() {
        assertFalse(lawnService.isValidPosition(6, 6, lawn));
    }

    @Test
    @DisplayName("Position with negative X-coordinate should be invalid")
    void isValidPosition_NegativeX_ReturnsFalse() {
        assertFalse(lawnService.isValidPosition(-1, 3, lawn));
    }

    @Test
    @DisplayName("Position with negative Y-coordinate should be invalid")
    void isValidPosition_NegativeY_ReturnsFalse() {
        assertFalse(lawnService.isValidPosition(3, -1, lawn));
    }

    @Test
    @DisplayName("Lawn with negative width should be considered invalid")
    void isValidPosition_NegativeWidth_ReturnsFalse() {
        Lawn negativeWidthLawn = new Lawn(-5, 5);
        assertFalse(lawnService.isValidPosition(3, 3, negativeWidthLawn));
    }

    @Test
    @DisplayName("Lawn with negative height should be considered invalid")
    void isValidPosition_NegativeHeight_ReturnsFalse() {
        Lawn negativeHeightLawn = new Lawn(5, -5);
        assertFalse(lawnService.isValidPosition(3, 3, negativeHeightLawn));
    }
}

